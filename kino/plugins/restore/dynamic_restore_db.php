<?php
$hostname = $_POST['hostname'];
$port = $_POST['port'];
$username = $_POST['username'];
$password = $_POST['password'];
$database = $_POST['database'];

// Connect to MySQL server with dynamic port
$conn = new mysqli($hostname, $username, $password, '', $port);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Check if the database exists
if (!$conn->select_db($database)) {
    // Create the database if it doesn't exist
    $createDatabaseQuery = "CREATE DATABASE $database";
    if ($conn->query($createDatabaseQuery) === TRUE) {
        echo "Database created successfully.<br>";

        // Close the connection and reconnect to the new database
        $conn->close();
        $conn = new mysqli($hostname, $username, $password, $database, $port);
    } else {
        echo "Error creating database: " . $conn->error . "<br>";
        $conn->close();
        exit;
    }
}

// Continue with the database restoration process
if (isset($_FILES['sql_file']) && $_FILES['sql_file']['error'] === UPLOAD_ERR_OK) {
    $file = $_FILES['sql_file']['tmp_name'];

    // Read SQL file
    $sql = file_get_contents($file);

    // Execute SQL statements
    if ($conn->multi_query($sql) === TRUE) {
        echo "Database restored successfully.";
    } else {
        echo "Error restoring database: " . $conn->error;
    }

    // Close the connection
    $conn->close();
} else {
    echo "Error uploading file.";
}
?>