$(document).ready(function () {
  var parent = getURLParameter('parent')
  console.log(parent)
  if (parent == null) {
    return false
  }
  $.ajax({
    url: 'http://localhost:8080/home/sideMenu?parent=' + parent, // Ganti 'url_endpoint_server_anda' dengan URL endpoint server Anda
    type: 'GET',
    dataType: 'json',
    success: function (data) {
      data.sideMenu.forEach(function (menu) {
        var elemenBaru = `
                    <li class="nav-item">
                        <a href="#" class="nav-link" onClick="(submenu(${menu.id}))">
                            ${menu.icon}
                            <p>
                                ${menu.ur_menu_title}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview ml-2">
                            <li class="nav-item">
                                <div id="${menu.id}"></div>
                            </li>
                        </ul>

                    </li>
                    `
        $('#container').append(elemenBaru)
      })
    },
    error: function (xhr, status, error) {
      console.error('Terjadi kesalahan saat memuat data AJAX: ', error)
    }
  })
})

function submenu(id) {
  if (id == '') {
    return false
  }

  var parent = id
  var baseurl = 'http://localhost:8080/'

  $.ajax({
    url: 'http://localhost:8080/home/sideMenu?parent=' + parent,
    type: 'GET',
    dataType: 'json',
    success: function (data) {
      if (data.sideMenu.length > 0) {
        data.sideMenu.forEach(function (menu) {
          var elements = $('[id="' + menu.id + '"]')
          var count = elements.length
          console.log(menu['link_menu'])
          if (menu['link_menu'] == '#') {
            var elemen = `
                            <li class="nav-item">
                                <a href="#" class="nav-link" onClick="(submenu(${menu['id']}))">
                                    ${menu['icon']}
                                    <p>
                                        ${menu['ur_menu_title']}
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>

                                <ul class="nav nav-treeview ml-2">
                                    <li class="nav-item">
                                        <div id="${menu['id']}"></div>
                                    </li>
                                </ul>
                            </li>
                        `
          } else {
            var elemen = `
                            <li class="nav-item">
                                <a href="${baseurl}${menu['link_menu']}" class="nav-link">
                                    ${menu['icon']}
                                    <p>
                                        ${menu['ur_menu_title']}
                                    </p>
                                </a>
                            </li>
                        `
          }
          $('#' + id).append(elemen)
        })
      } else {
        $('#plush').html('')
      }
    },
    error: function (xhr, status, error) {
      console.error('Terjadi kesalahan saat memuat data AJAX: ', error)
    }
  })
}

function getURLParameter(name) {
  const urlParams = new URLSearchParams(window.location.search)
  return urlParams.get(name)
}
