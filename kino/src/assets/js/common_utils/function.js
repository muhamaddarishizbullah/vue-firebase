function handle_file_upload(event, callback) {
  var file = event.target.files[0]
  var reader = new FileReader()

  // Validasi jenis file
  var allowedExtensions = ['.xls', '.xlsx']
  var fileExtension = file.name.split('.').pop()
  if (!allowedExtensions.includes('.' + fileExtension)) {
    alert(
      'Uploaded file type is not supported. Please select a file with an .xls or .xlsx extension.'
    )
    return
  }

  // Memanggil fungsi untuk menampilkan nama file
  display_file_name(file.name)

  reader.onload = function (e) {
    var data = e.target.result
    var workbook = XLSX.read(data, { type: 'binary' })
    var sheetName = workbook.SheetNames[0]
    localSheetData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName])

    // Do something with the JSON data
    //console.log(sheetData);
  }

  reader.readAsBinaryString(file)
}

// Fungsi untuk menampilkan nama file
function display_file_name(name) {
  var fileNameElement = $('.custom-file-label')
  fileNameElement.text(name)
}

function convert_json_to_excel(url) {
  // Melakukan permintaan AJAX untuk mengambil data JSON dari URL
  $.ajax({
    url: url,
    method: 'GET',
    dataType: 'json',
    success: function (response) {
      // Mengonversi data JSON menjadi file Excel
      var workbook = XLSX.utils.book_new()
      $.each(response, function (sheetName, sheetData) {
        var worksheet = XLSX.utils.json_to_sheet(sheetData)
        XLSX.utils.book_append_sheet(workbook, worksheet, sheetName)
      })

      // Mengonversi workbook ke bentuk array buffer
      var wbout = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' })

      // Membuat blob dari array buffer
      var blob = new Blob([wbout], { type: 'application/octet-stream' })

      // Membuat URL untuk file Excel dan membuat link untuk mengunduhnya
      var url = URL.createObjectURL(blob)
      var a = document.createElement('a')
      a.href = url
      a.download = 'data.xlsx' // Nama file Excel yang diunduh
      a.click()

      // Membersihkan URL yang sudah tidak diperlukan
      setTimeout(function () {
        URL.revokeObjectURL(url)
      }, 100)
    },
    error: function (xhr, status, error) {
      console.error('There was an error fetching JSON data:', error)
    }
  })
}

function process_sheet_data() {
  // Lakukan operasi atau manipulasi dengan localSheetData di sini
  if (localSheetData) {
    console.log('Data dari localSheetData:', localSheetData)

    // Misalnya, tampilkan data di dalam sebuah tabel HTML
    var tableHtml = '<table>'
    $.each(localSheetData, function (index, row) {
      tableHtml += '<tr>'
      $.each(row, function (key, value) {
        tableHtml += '<td>' + value + '</td>'
      })
      tableHtml += '</tr>'
    })
    tableHtml += '</table>'

    $('#dataTable').html(tableHtml) // Menampilkan data di dalam elemen dengan id="dataTable"
  } else {
    console.log('Belum ada data sheet yang diunggah.')
  }
}

function convert_json_to_sql(jsonData) {
  var sql = ''

  jsonData.forEach(function (record) {
    var keys = Object.keys(record)
    var values = Object.values(record)

    // SQL INSERT INTO query
    sql += 'INSERT INTO table_name (' + keys.join(', ') + ') VALUES ('

    // Nilai dari setiap kolom
    values.forEach(function (value, index) {
      // Handle tipe data string dengan tambahan tanda kutip tunggal
      if (typeof value === 'string') {
        value = "'" + value + "'"
      }
      // Nilai null atau undefined diubah menjadi NULL dalam SQL
      else if (value === null || value === undefined) {
        value = 'NULL'
      }

      // Tambahkan koma untuk nilai selain yang pertama
      if (index > 0) {
        sql += ', '
      }

      sql += value
    })

    sql += ');\n' // Akhiri query dengan tanda titik koma dan baris baru
  })
  return sql
}

function convertJsonToSql(sortedJsonData) {
  var sqlStatements = []

  // Loop melalui objek dalam sortedJsonData
  for (var tableName in sortedJsonData) {
    if (sortedJsonData.hasOwnProperty(tableName)) {
      var tableData = sortedJsonData[tableName]

      // Loop melalui setiap baris dalam tabel
      for (var i = 0; i < tableData.length; i++) {
        var rowData = tableData[i]

        // Mengambil nama kolom dari properti objek pertama
        var columns = Object.keys(rowData).join(', ')

        // Mengambil nilai dari setiap kolom dan memformatnya sesuai tipe data di SQL
        var values = Object.values(rowData)
          .map((value) => {
            if (typeof value === 'string') {
              return "'" + value.replace(/'/g, "''") + "'"
            } else if (value === null) {
              return 'NULL'
            } else {
              return value
            }
          })
          .join(', ')

        // Membuat statement SQL INSERT dan menambahkannya ke dalam array
        var sqlStatement = `INSERT INTO ${tableName} (${columns}) VALUES (${values});`
        sqlStatements.push(sqlStatement)
      }
    }
  }

  // Menggabungkan semua statement SQL menjadi satu string dengan pemisah baru (\n)
  var sqlData = sqlStatements.join('\n')

  return sqlData
}

var customTableOrder = [
  'years',
  'auth_groups',
  'auth_permissions_menus',
  'master_table',
  'migrations',
  'users',
  'auth_permissions',
  'auth_activation_attempts',
  'auth_groups_permissions',
  'auth_groups_users',
  'auth_logins',
  'auth_permissions_relations',
  'auth_reset_attempts',
  'auth_tokens',
  'auth_users_permissions'
]

function customTableSort(a, b) {
  return customTableOrder.indexOf(a) - customTableOrder.indexOf(b)
}

function sortTables(jsonData) {
  // Ambil properti-properti objek dari JSON dan simpan dalam array
  var tableNames = Object.keys(jsonData)

  // Urutkan array tableNames berdasarkan urutan khusus yang telah ditentukan
  tableNames.sort(customTableSort)

  // Buat objek baru untuk menyimpan data yang telah diurutkan
  var sortedJsonData = {}

  // Pindahkan data tabel yang telah diurutkan ke dalam objek baru
  for (var i = 0; i < tableNames.length; i++) {
    var tableName = tableNames[i]
    sortedJsonData[tableName] = jsonData[tableName]
  }

  return sortedJsonData
}

function make_data_table(table, url, columnsTable) {
  var columnsData = []
  columnsData.push({
    data: 'no',
    orderable: false
  })
  columnsTable.forEach(function (item) {
    var column = {
      data: item
    }

    columnsData.push(column)
  })
  columnsData.push({
    data: 'id',
    render: function (data, type, row) {
      var data = JSON.stringify(row)
      var editButton = $('<button>', {
        class: 'btn btn-warning',
        'data-id': row.id,
        'data-csrf_token_erenggar': row.csrf_token_erenggar,
        text: 'Edit'
      })
      var deleteButton = $('<button>', {
        class: 'btn btn-danger',
        'data-id': row.id,
        'data-csrf_token_erenggar': row.csrf_token_erenggar,
        text: 'Delete'
      })
      columnsTable.forEach(function (attribute) {
        editButton.attr('data-' + attribute, row[attribute])
      })
      var rowHtml = editButton.prop('outerHTML') + ' ' + deleteButton.prop('outerHTML')
      return rowHtml
    }
  })
  var read_table = $('#' + table).DataTable({
    processing: true,
    serverSide: true,
    autoWidth: false,
    ajax: url,
    order: [],
    dom: 'Bfrtip',
    buttons: ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
    columns: columnsData
  })
  read_table.on('draw', function () {
    var rowData = read_table.row(0).data()
    var csrfMetaElement = $('meta[name="csrf-token"]')
    console.log(rowData.csrf)
    csrfMetaElement.attr('content', rowData.csrf)
  })
}

function capitalize_first_letter(text) {
  return text.charAt(0).toUpperCase() + text.slice(1)
}

function edit_table(url, result, table) {
  $.post({
    url: url,
    data: result.value,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    }
  })
    .done(function (response) {
      Swal.fire({
        icon: 'success',
        title: 'Edit Successful',
        text: response['message']
      })
      $('#' + table)
        .DataTable()
        .ajax.reload()
    })
    .fail(function (xhr, textStatus, errorThrown) {
      var errorData = JSON.parse(xhr.responseText)
      Swal.fire({
        icon: 'error',
        title: errorData.title,
        text: errorData.message
      })
    })
}

function full_edit_table(element, url_edit, columnsTable, table) {
  var dataValues = {}
  var id = $(element).attr('data-id')
  var csrf_token_name = 'csrf_token_erenggar'
  var csrf_token_hash = $(element).attr('data-' + csrf_token_name)
  columnsTable.forEach(function (item) {
    var value = $(element).attr('data-' + item)
    dataValues[item] = value
  })
  dataValues['id'] = id
  var html = ''
  columnsTable.forEach(function (item) {
    html +=
      `
                    <div class="form-group">
                        <label for="` +
      item +
      `">` +
      capitalize_first_letter(item) +
      `:</label>
                        <input type="text" class="form-control" id="` +
      item +
      dataValues['id'] +
      `" value="` +
      dataValues[item] +
      `" required>
                    </div>
                `
  })
  Swal.fire({
    icon: 'warning',
    title: 'Edit',
    text: 'please do it carefully.',
    html: html,
    showCancelButton: true,
    confirmButtonText: 'Save',
    cancelButtonText: 'Close',
    preConfirm: () => {
      columnsTable.forEach(function (item) {
        var value = Swal.getPopup().querySelector('#' + item + id).value
        if (!value) {
          Swal.showValidationMessage('Please fill in all fields.')
          return false
        }
        dataValues[item] = value
      })
      return dataValues
    }
  }).then((result) => {
    if (result.isConfirmed) {
      result.value[csrf_token_name] = csrf_token_hash
      result.value.id = id
      edit_table(url_edit, result, table)
    }
  })
}

function delete_table(element, url_delete, table) {
  var id = $(element).attr('data-id')
  var csrf_token_name = 'csrf_token_erenggar'
  var csrf_token_hash = $(element).attr('data-' + csrf_token_name)
  Swal.fire({
    icon: 'warning',
    title: 'Delete Confirmation',
    text: 'Are you sure you want to delete this data?',
    showCancelButton: true,
    confirmButtonText: 'Delete',
    cancelButtonText: 'Cancel'
  }).then((result) => {
    if (result.isConfirmed) {
      $.post({
        url: url_delete,
        data: {
          id: id,
          [csrf_token_name]: csrf_token_hash
        },
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        }
      })
        .done(function (response) {
          Swal.fire({
            icon: 'success',
            title: 'Delete Successful',
            text: response['message']
          })
          $('#' + table)
            .DataTable()
            .ajax.reload()
        })
        .fail(function (xhr, textStatus, errorThrown) {
          var errorData = JSON.parse(xhr.responseText)
          Swal.fire({
            icon: 'error',
            title: errorData.title,
            text: errorData.message
          })
        })
    }
  })
}

function create_table(url_create, columnsTable, table) {
  var dataValues = {}
  var csrf_token_hash = $('[name=csrf-token]').attr('content')
  var csrf_token_name = 'csrf_token_erenggar'
  var dataTableRows = $('#' + table)
    .DataTable()
    .rows()
    .count()
  if (dataTableRows > 0) {
    var firstRow = $('#' + table)
      .DataTable()
      .row(0)
      .data()
    csrf_token_hash = firstRow.csrf_token_erenggar
  }
  var html = ''
  columnsTable.forEach(function (item) {
    html +=
      `
                    <div class="form-group">
                        <label for="` +
      item +
      `">` +
      capitalize_first_letter(item) +
      `:</label>
                        <input type="text" class="form-control" id="` +
      item +
      `" required>
                    </div>
                `
  })
  Swal.fire({
    icon: 'success',
    title: 'Create',
    text: 'Please provide the necessary details for the new data.',
    html: html,
    showCancelButton: true,
    confirmButtonText: 'Save',
    cancelButtonText: 'Close',
    preConfirm: () => {
      columnsTable.forEach(function (item) {
        var value = Swal.getPopup().querySelector('#' + item).value
        if (!value) {
          Swal.showValidationMessage('Please fill in all fields.')
          return false
        }
        dataValues[item] = value
      })
      return dataValues
    }
  }).then((result) => {
    if (result.isConfirmed) {
      result.value[csrf_token_name] = csrf_token_hash
      create_table_end(url_create, result, table)
    }
  })
}

function create_table_end(url_create, result, table) {
  $.post({
    url: url_create,
    data: result.value,
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    }
  })
    .done(function (response) {
      Swal.fire({
        icon: 'success',
        title: 'Create Successful',
        text: response['message']
      })
      $('#' + table)
        .DataTable()
        .ajax.reload()
    })
    .fail(function (xhr, textStatus, errorThrown) {
      var errorData = JSON.parse(xhr.responseText)
      Swal.fire({
        icon: 'error',
        title: errorData.title,
        text: errorData.message
      })
    })
}

function alertSwalSuccess(message) {
  Swal.fire({
    icon: 'success',
    title: 'Successful',
    text: message
  })
}

function alertSwalFailed(message) {
  Swal.fire({
    icon: 'error',
    title: 'Failed',
    text: message
  })
}
