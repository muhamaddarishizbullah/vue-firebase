import { firebaseConfig, firebaseApp, analytics } from '../middleware/firebaseConfig'
import { getAuth, GoogleAuthProvider, signInWithPopup, onAuthStateChanged } from 'firebase/auth'
import { createRouter, createWebHistory } from 'vue-router'
import ScaleUpValidationView from '../../src/views/ScaleUpValidationView.vue'
import PageView from '../../src/views/PageView.vue'
import LoginView from '../../src/views/LoginView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: PageView
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../../src/views/AboutView.vue')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: PageView
    },
    {
      path: '/scale-up-dan-validasi',
      name: 'scale-up-dan-validasi',
      component: ScaleUpValidationView
    },
    {
      path: '/dokumen-berjalan',
      name: 'dokumen-berjalan',
      component: PageView
    }
  ]
})

const auth = getAuth()
const provider = new GoogleAuthProvider()
onAuthStateChanged(auth, (user) => {
  if (user) {
    // Pengguna sudah login, biarkan akses ke halaman lain
  } else {
    router.push('/login')
  }
})

export default router
export { router }
