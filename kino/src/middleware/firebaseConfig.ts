import { initializeApp } from 'firebase/app'
import { getAnalytics } from 'firebase/analytics'
import { getDatabase } from 'firebase/database'
import { initializeAppCheck, ReCaptchaV3Provider } from 'firebase/app-check'

const firebaseConfig = {
  apiKey: 'AIzaSyDDkT53OEgldgoFWFq_seTwmssO3m-uuSc',
  authDomain: 'kino2-2c150.firebaseapp.com',
  databaseURL: 'https://kino2-2c150-default-rtdb.firebaseio.com',
  projectId: 'kino2-2c150',
  storageBucket: 'kino2-2c150.appspot.com',
  messagingSenderId: '184450143806',
  appId: '1:184450143806:web:40248f9669c39709fb636f',
  measurementId: 'G-65RG7Y0436'
}

// Inisialisasi Firebase App
const firebaseApp = initializeApp(firebaseConfig)

// Pengaktifan Analytics (opsional)
const analytics = getAnalytics(firebaseApp)

const database = getDatabase(firebaseApp)

const appCheck = initializeAppCheck(firebaseApp, {
  provider: new ReCaptchaV3Provider('6LdplGEpAAAAAB7JXGUQY4Y7hfFPHApgTBhId2In'),

  // Optional argument. If true, the SDK automatically refreshes App Check
  // tokens as needed.
  isTokenAutoRefreshEnabled: true
})
// Ekspor konfigurasi Firebase
export { firebaseConfig, firebaseApp, analytics, database, appCheck }
